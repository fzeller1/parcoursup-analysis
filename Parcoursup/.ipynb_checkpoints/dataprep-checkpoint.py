import pandas as pd
from columns import col_names

def get_pct(df, part, total):
    """
    Part and total are two columns name from df
    Returns a Serie of percentage of part-column in total-column
    """
    return df[part] / df[total]

def get_subtotal(df, value, level):
    """
    df is a multi-indexed dataframe. value is the column with the measures to be summed on index.
    Return df with a new column: the subtotals of value along the first hierarchy index.
    """
    level_name = "total_" + level
    for lvl in df.index.get_level_values(0).unique():
        sum_lvl = df.loc[lvl, value].sum()
        df.loc[lvl, level_name] = sum_lvl
		
def import_data():
	df_raw = pd.read_csv("Data/fr-esr-parcoursup.csv", sep=";")
	df_city = pd.read_csv("Data/communes2020.csv")
	return df_raw, df_city
	
def clean_data_raw(df):
	df_raw = df.rename(col_names, axis=1)
	
	#Cleaning STAPS
	staps_mask = df_raw["formation_type_detailed"].str.contains("STAPS") == True
	df_raw.loc[staps_mask, "formation_type"] = "Licence - STAPS"
	df_raw.loc[
		staps_mask, "formation_type_precise"
	] = "Sciences et Techniques des Activités Physiques et Sportives"

	#Cleaning typos
	df_raw["formation_type_precise"] = df_raw["formation_type_precise"].replace(
		"Cycle pluridisciplinaire d?'enseignement supérieur - Humanités",
		"Cycle pluridisciplinaire d'enseignement supérieur - Humanités",
	)

	df_raw["formation_capacity"] = df_raw["formation_capacity"].fillna(0)
	
	#To category
	ind_to_cat = [1, 3, 4, 5, 6, 7, 8, 9, 10]
	to_cat = list(df_raw.columns[ind_to_cat])
	df_raw[to_cat] = df_raw[to_cat].astype("category")

	ind_to_int = [15]
	to_int = list(df_raw.columns[ind_to_int])
	df_raw[to_int] = df_raw[to_int].astype("int64")
	
	df_raw["total_applications_bac_general"] = (
    df_raw["main_applications_bac_general"] + df_raw["add_applications_bac_general"]
)
	df_raw["total_applications_bac_techno"] = (
    df_raw["main_applications_bac_techno"] + df_raw["add_applications_bac_techno"]
)
	df_raw["total_applications_bac_pro"] = (
    df_raw["main_applications_bac_pro"] + df_raw["add_applications_bac_pro"]
)

	df_raw["total_applications_other"] = (
    df_raw["main_applications_other"] + df_raw["add_applications_other"]
)

	df_raw["pct_admitted"] = df_raw["admitted_applications"] / df_raw["total_applications"]

	df_raw["pct_total_f_applications"] = (
    df_raw["total_f_applications"] / df_raw["total_applications"]
)
	
	return df_raw